beautifulsoup4==4.4.1
bs4==0.0.1
dnspython==1.14.0
ipaddr==2.1.11
ipwhois==0.13.0
requests==2.10.0
json2html==1.0.1
futures==3.0.5
