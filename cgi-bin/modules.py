#!/usr/bin/python
##################################################
#    ____     _   ____          __           __  #
#   / __/__ _(_) / __/__ ____  / /____  ___ / /  #
#  _\ \/ _ `/ / _\ \/ _ `/ _ \/ __/ _ \(_-</ _ \ #
# /___/\_,_/_/ /___/\_,_/_//_/\__/\___/___/_//_/ #
#                                                #
#             TWITTER : @SASH243                 #
##################################################
import requests
import json
from json2html import *
from ConfigParser import SafeConfigParser
requests.packages.urllib3.disable_warnings()



# Loading the configuration file
parser = SafeConfigParser()
parser.read('/data/lookup-tool/configuration.ini')

# Function for the Alienvault results
def alienvault_otx(ip):
	key = parser.get('ALIENVAULT-OTX', 'api')
	try:
		a = requests.get('https://otx.alienvault.com/api/v1/indicators/IPv4/'+ip+'/reputation',headers={'X-OTX-API-KEY':key})
		if ((a.status_code == 200) & (a.json()['reputation'] != None)):
			result = a.json()['reputation']
			pass
		else:
			result = {"error":"No information available."} 
			pass
		pass
	except:
		result = {"error":"No information available."}
		pass

	htmlblock = '''<p>
				   <h2>Alienvault-OTX Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = alienvault_otx('69.73.130.198')


def WhoisInfo(ip):
	from ipwhois import IPWhois
	try:
		result = IPWhois(ip).lookup_whois()
		pass
	except:
		result = {"error":"No information available."}
		pass

	htmlblock = '''<p>
				   <h2>WhoIs Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = WhoisInfo('69.73.130.198')


def ShodanInfo(ip):
	key = parser.get('SHODAN', 'api')
	try:
		result = requests.get('https://api.shodan.io/shodan/host/'+ip+'?key='+key+'&minify=True').json()
		pass
	except:
		result = {"error":"No information available."}
		pass

	htmlblock = '''<p>
				   <h2>Shodan Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = ShodanInfo('8.8.8.8')


def MalwrInfo(ip):
	from MalwrAPI import MalwrAPI
	username = parser.get('MALWR', 'username')
	password = parser.get('MALWR', 'password')
	try:
		result = MalwrAPI(True, username,password).search("ip:"+ip)
		pass
	except:
		result = [{"error":"No information available."}]
		pass
	block = ''
	for item in result:
		block = block + json2html.convert(json = item)
	htmlblock = '''<p>
				   <h2>Malwr Information</h2>
				'''+block+'''
				</p>'''

	return htmlblock	

# a = MalwrInfo('8.8.8.8')


def VirusTotalInfo(ip):
	key = parser.get('VIRUSTOTAL', 'api')
	try:
		result =  requests.get("https://www.virustotal.com/vtapi/v2/ip-address/report", params = {"ip": ip, "apikey": key}).json()
		pass
	except:
		result = {"error":"No information available."}
		pass

	htmlblock = '''<p>
				   <h2>VirusTotal Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = VirusTotalInfo('188.40.75.132')


def DshieldInfo(ip):
	try:
		result =  requests.get("http://isc.sans.edu/api/ip/"+ip+"?json").json()
		pass
	except:
		result = {"error":"No information available."}
		pass

	htmlblock = '''<p>
				   <h2>ISC DShield Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = DshieldInfo('188.40.75.132')


def ThreatCrowdInfo(ip):
	try:
		result =  requests.get("https://www.threatcrowd.org/searchApi/v2/ip/report/", params = {"ip": ip}).json()
		pass
	except:
		result = {"error":"No information available."}
		pass	

	htmlblock = '''<p>
				   <h2>ThreatCrowd Information</h2>
				'''+json2html.convert(json = result)+'''
				</p>'''

	return htmlblock

# a = ThreatCrowdInfo('188.40.75.132')


def IPvoidInfo(ip):
	from bs4 import BeautifulSoup
	try:
		requests.get('http://www.ipvoid.com/update-report/'+ip)
		a = requests.get('http://www.ipvoid.com/scan/'+ip)
		if ((a.status_code == 200) & ((a.text).find('Report not found') == -1)):
			soup = BeautifulSoup(a.text, "html.parser")
			data = soup.find_all("table")

			result = ('<b>Analysis Details</b>'+str(data[0])+'<br>'+str(data[1])).decode("ascii",'ignore')
			pass
		else:
			result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
			pass
		pass
	except:
		result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
		pass

	htmlblock = '''<p>
				   <h2>IPvoid Information</h2>
				'''+result+'''
				</p>'''

	return htmlblock

# a = IPvoidInfo('188.40.75.132')

def RobtexGraph(ip):

	result = '''<img src="https://www.robtex.com/gfx/graph.png?dns='''+ip+'''" alt="No information available."> '''
	htmlblock = '''<p>
				   <h2>Robtex Information</h2>
				'''+result+'''
				</p>'''

	return htmlblock

# a = RobtexGraph('188.40.75.132')

def MalwareDomainListInfo(ip):
	from bs4 import BeautifulSoup
	try:
		a = requests.get('http://www.malwaredomainlist.com/mdl.php?search='+ip+'&colsearch=All&quantity=50&inactive=on')
		if (a.status_code == 200):
			soup = BeautifulSoup(a.text, "html.parser")
			data = soup.find_all("table")

			result = str(data[1]).decode("ascii",'ignore')
			pass
		else:
			result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
			pass
		pass
	except:
		result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
		pass

	htmlblock = '''<p>
				   <h2>Malware Domain List Information</h2>
				'''+result+'''
				</p>'''

	return htmlblock

# a = MalwareDomainListInfo('188.40.75.132')

def MalwareTrafficAnalysis(ip):
	from bs4 import BeautifulSoup
	try:
		a = requests.get('https://www.google.co.in/search?hl=en&as_epq='+ip+'&as_sitesearch=malware-traffic-analysis.net')
		if (a.status_code == 200):
			soup = BeautifulSoup(a.text, "html.parser")
			data = soup.find_all("div", {"id": "center_col"})

			result = str(data[0]).decode("ascii",'ignore')
			pass
		else:
			result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
			pass
		pass
	except:
		result = '''<table border="1"><tr><th>error</th><td>No information available.</td></tr></table>'''
		pass

	htmlblock = '''<p>
				   <h2>Malware Traffic Analysis Information</h2>
				'''+result+'''
				</p>'''

	return htmlblock

# a = MalwareTrafficAnalysis('188.0.236.9')