FROM alpine:latest
MAINTAINER Sai Santosh <ssanthosh636@gmail.com>

RUN apk update
RUN apk add apache2 python py-pip

RUN rm -rf /var/www/localhost/cgi-bin && \
	sed -i 's$#LoadModule cgi_module modules/mod_cgi.so$LoadModule cgi_module /usr/lib/apache2/mod_cgi.so$g' /etc/apache2/httpd.conf && \
	sed -i 's/^#ServerName.*/ServerName localhost/' /etc/apache2/httpd.conf && \
	sed -i 's#AllowOverride none#AllowOverride All#' /etc/apache2/httpd.conf && \
	mkdir /run/apache2 && \
	mkdir -p /data/lookup-tool/

COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

ADD cgi-bin /var/www/localhost/cgi-bin/
RUN chmod +x /var/www/localhost/cgi-bin/*

VOLUME ["/data/lookup-tool/"]

EXPOSE 80

ENTRYPOINT /usr/sbin/httpd -D FOREGROUND